# claire-lauvernet-R-mkl-4.0.4

Claire Lauvernet, container R mkl en 4.0.4

## HELP
The singularity image is delivered with =>
```
R-4.0.4
intel-mkl-64bit-2018.2-046
c("DiceEval","DiceKriging","deepgp","tidyr","dplyr")
```
## BUILD
```
singularity build r-mkl-4.0.4.sif r-mkl-4.0.4.def
```

## USAGE
First, pull the image then use it like so =>
```
singularity pull r-mkl.sif oras://registry.forgemia.inra.fr/singularity-mesolr/claire-lauvernet-r-mkl-4.0.4/claire-lauvernet-r-mkl-4.0.4:latest
./r-mkl.sif file.R
```
